import os
import pathlib

from minio import Minio
from dotenv import load_dotenv

load_dotenv()

DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw', 'market_sales.csv')
FILE_NAME = pathlib.Path(RAW_DATA_PATH).name

MINIO_BUCKET = os.environ.get("MINIO_BUCKET")
MINIO_ACCESS_KEY = os.environ.get("MINIO_ACCESS_KEY")
MINIO_SECRET_KEY = os.environ.get("MINIO_SECRET_KEY")
MINIO_API_HOST = os.environ.get("MINIO_API_HOST")
print(MINIO_API_HOST)
MINIO_CLIENT = Minio(MINIO_API_HOST, access_key=MINIO_ACCESS_KEY, secret_key=MINIO_SECRET_KEY, secure=False)

def upload_data(bucket=MINIO_BUCKET, source_path=RAW_DATA_PATH):
    found = MINIO_CLIENT.bucket_exists(MINIO_BUCKET)
    if not found:
       MINIO_CLIENT.make_bucket(MINIO_BUCKET)
    else:
       print(f"Bucket {MINIO_BUCKET} already exists")    
    MINIO_CLIENT.fput_object(MINIO_BUCKET, FILE_NAME,
                             RAW_DATA_PATH)    
    print(f"File {FILE_NAME} is successfully uploaded to {MINIO_BUCKET}")
    
if __name__ == "__main__":
    upload_data()