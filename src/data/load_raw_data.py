import pandas as pd
import os
from pathlib2 import Path
from clearml import Task, Dataset
import download_data as dd
from dotenv import load_dotenv

load_dotenv()
MINIO_BUCKET = os.environ.get("MINIO_BUCKET")

DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(*[DATA_PATH, 'raw', 'market_sales.csv'])



def load_raw_data(bucket=MINIO_BUCKET, local_data_path=RAW_DATA_PATH):
    """

    :param data_path: local path where dataset is located
    :return: 
    """
    # create an dataset experiment
task = Task.init(project_name="mlops-rec-sys",
                 task_name="Step1 load_raw_data",
                 task_type=Task.TaskTypes.data_processing,
                 tags=['raw', 'dataset','v2']
                 )
# for check if code work properly
# Task.set_offline(True)

conf = {
        'bucket': MINIO_BUCKET,
        'local_data_path': RAW_DATA_PATH,
        }
print('Configuration: {}'.format(conf))
conf = task.connect(conf)
# only create the task, we will actually execute it later
# task.execute_remotely(queue_name="default")
bucket = conf.get('bucket', MINIO_BUCKET)
data_path = conf.get('local_data_path', RAW_DATA_PATH)
# local dataset
if dd.download_data(bucket, data_path):

    # add and upload local file containing our  dataset
    dataset = Dataset.create(
    dataset_name='raw-data',
    dataset_project="mlops-rec-sys",
    dataset_tags=['dataset v2'],
    parent_datasets=None,
    use_current_task=False,
    )
    conf['dataset_id'] = dataset.id
    dataset.add_files(data_path)
    filepath = os.path.join(Path(data_path).name)
    print(filepath)
    print(data_path)
    df = pd.read_csv(data_path)
    descr_df = pd.DataFrame(df.describe(datetime_is_numeric=True, include='all').T)

    logger = dataset.get_logger()
    logger.report_table(
                    title="Data-raw-head",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=df.head(),
                        )
    logger.report_table(
                    title="Data-raw-description",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=descr_df,
                        )
    dataset.upload(show_progress=True)
    print('Uploading artifacts in the background ...')
    # we are done
    print('Done')
    dataset.finalize()



if __name__ == '__main__':

    load_raw_data()