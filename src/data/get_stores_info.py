import pandas as pd
import numpy as np
import streamlit as st
from typing import List
import click

#NB cost changed to sum_sale
# @click.command()
# @click.argument('input_path', type=click.Path(exists=True))
# @click.argument('output_path', type=click.Path())
def get_stores_info(cleaned_data: pd.DataFrame):
    
    # cleaned_data = pd.read_csv(input_path)
    stores = cleaned_data.groupby('store_id').agg({'item_id': ['nunique', 'count'],
                                                   'user_id': 'nunique',
                                                   'sum_sale': 'sum'}).reset_index(col_level = 1)
    stores.item_id['nunique'].rename('unique_goods', inplace = True)
    stores = stores.rename(columns = {'count': 'total_sales',
                                      'nunique': 'unique_clients',
                                      'sum': 'total_amount'}).droplevel(level = 0, axis = 1)
    # stores.to_csv(output_path, index = False)
    return stores

if __name__ == '__main__':
    get_stores_info(df)
