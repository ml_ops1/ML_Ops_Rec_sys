"""
add because pylint
This module load and clean raw data
"""
# -*- coding: utf-8 -*-
import os
from pathlib2 import Path
import pandas as pd
import numpy as np
import streamlit as st
from typing import List
# import click
import glob
# import pickle
from clearml import Task, Dataset
import clean as cl
import user_license as ul
import get_stores_info as gsi
import get_goods_data as ggd


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(*[DATA_PATH, 'raw', 'market_sales.csv'])
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')

# URL = 'https://link.storjshare.io/s/jwc5svdn2t7v7gbby6c3zvcivdcq/database/market_sales.csv'
# URL = 'https://www.dropbox.com/s/nhswkhngncsi8n6/Data_rec_sys_row.csv?dl=1'
# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
def clean_data(input_path=RAW_DATA_PATH, output_path=CLEANED_DATA_PATH):
    # Connecting ClearML with the current process,
    # from here on everything is logged automatically
    task = Task.init(project_name="mlops-rec-sys",
                    task_name="Step2 clean_data",
                    task_type=Task.TaskTypes.data_processing,
                    tags=['clean', 'dataset','v2'])
    
    # for check if code work properly
    # Task.set_offline(True)
    # task.execute_remotely(queue_name="default")

    # program arguments
    # Use either dataset_task_id to point to a tasks artifact or
    # use a direct url with dataset_url
    conf = {
        'dataset_id':'',
        'project_name': 'mlops-rec-sys',
        'source_dataset_name': 'raw-data',
        'input_path': RAW_DATA_PATH,
        'output_path': CLEANED_DATA_PATH
        }
    print('Configuration: {}'.format(conf))
    conf = task.connect(conf)
    
    if conf['dataset_id']:
        dataset = Dataset.get(dataset_id=conf.get('dataset_id', ''))
    else:
        dataset = Dataset.get(dataset_project=conf.get('project_name', "mlops-rec-sys"),
                            dataset_name=conf.get('source_dataset_name', 'raw-data'))

    input_path = dataset.get_local_copy() 

    clean_dataset = Dataset.create(
    dataset_name='clean-data',
    dataset_project='mlops-rec-sys',
    dataset_tags=['clean dataset v2'],
    parent_datasets=None,
    use_current_task=False,
    )
    conf['out_clean_dataset_id'] = clean_dataset.id
    logger = clean_dataset.get_logger()


    # "process" data
    # input_path = os.path.join(RAW_DATA_PATH, 'market_sales.csv')
    cleaned_data = []
    user_license_list = []
    for i, filename in enumerate(glob.glob(os.path.join(input_path, '*.csv')), 1):

        new_f = Path(filename).name
        cleaned_df = cl.clean(filename)
        user_license_list.append(ul.user_license(cleaned_df))
        output_file_path = os.path.join(output_path, f'cleaned_{new_f}')
        cleaned_df.to_csv(output_file_path, index=False)
        clean_dataset.add_files(output_file_path)
        descr_df_num = pd.DataFrame(cleaned_df.describe(datetime_is_numeric=True,
                                                    include=None).T)
        logger.report_table(
                    title=f"Cleaned-numerical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_num,
                        )
        descr_df_cat = pd.DataFrame(cleaned_df.describe(datetime_is_numeric=True,
                                                    include=object).T)
        logger.report_table(
                    title=f"Cleaned-categorical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_cat,
                        )
        logger.report_table(
                    title=f"Cleaned-head-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=cleaned_df.head(),
                        )        
        cleaned_data.append(cleaned_df)

    clean_dataset.upload(show_progress=True)
    user_license_df = pd.concat(user_license_list)
    user_license_df.columns = ['user_id', 'license']
    user_license_df = ul.user_license(user_license_df)
    metadata = pd.DataFrame(user_license_df.client_license.value_counts()).to_dict()
    task.register_artifact('user_license', user_license_df, metadata=metadata)

    final_df = pd.concat(cleaned_data)
    final_df.to_csv(os.path.join(output_path, 'cleaned_all.csv'), index=False)
    descr_df_num = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=None).T)
    logger.report_table(
                title=f"Cleaned-numerical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_num,
                    )
    descr_df_cat = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=object).T)
    logger.report_table(
                title=f"Cleaned-categorical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_cat,
                    )
    # clean_dataset.upload(show_progress=True)
    clean_dataset.finalize()

    stores_info_dataset = Dataset.create(
        dataset_name='stores-info-data',
        dataset_project='mlops-rec-sys',
        dataset_tags=['stores v1'],
        parent_datasets=None,
        use_current_task=False,
        )
    conf['out_stores_info_dataset_id'] = stores_info_dataset.id
    logger = stores_info_dataset.get_logger()
    stores_df = gsi.get_stores_info(final_df)
    stores_df.to_csv(os.path.join(output_path, 'stores.csv'), index=False)
    descr_df_num = pd.DataFrame(stores_df.describe(datetime_is_numeric=True,
                                                include=None).T)
    logger.report_table(
                title=f"Stores-numerical-description",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_num,
                    )
    descr_df_cat = pd.DataFrame(stores_df.describe(datetime_is_numeric=True,
                                                include=object).T)
    logger.report_table(
                title=f"Stores-categorical-description",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_cat,
                    )
    logger.report_table(
                    title=f"Stores-head",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=stores_df.head(),
                        )

    stores_info_dataset.upload(show_progress=True)
    stores_info_dataset.finalize()
    
    goods_dataset = Dataset.create(
        dataset_name='goods-data',
        dataset_project='mlops-rec-sys',
        dataset_tags=['goods v1'],
        parent_datasets=None,
        use_current_task=False,
        )
    conf['out_goods_dataset_id'] = goods_dataset.id
    logger = goods_dataset.get_logger()
    goods_df = ggd.get_goods_data(final_df)
    goods_df.to_csv(os.path.join(output_path, 'goods.csv'), index=False)
    descr_df_num = pd.DataFrame(goods_df.describe(datetime_is_numeric=True,
                                                include=None).T)
    logger.report_table(
                title=f"Goods-numerical-description",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_num,
                    )
    descr_df_cat = pd.DataFrame(goods_df.describe(datetime_is_numeric=True,
                                                include=object).T)
    logger.report_table(
                title=f"Goods-categorical-description",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_cat,
                    )
    logger.report_table(
                    title=f"Goods-head",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=goods_df.head(),
                        )

    goods_dataset.upload(show_progress=True)
    goods_dataset.finalize()

if __name__ == "__main__":

    clean_data()
