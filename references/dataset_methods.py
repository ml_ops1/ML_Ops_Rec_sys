#!/usr/bin/env python3
##########################################################################
for path in os.scandir(dataset_path):
    if path.is_file():
        print(path.name)
path_methods = ['inode',
                'is_dir',
                'is_file',
                'is_symlink',
                'name',
                'path',
                'stat']
##########################################################################
# Signature: glob.glob(pathname, *, recursive=False)
# Docstring:
# Return a list of paths matching a pathname pattern.

# The pattern may contain simple shell-style wildcards a la
# fnmatch. However, unlike fnmatch, filenames starting with a
# dot are special cases that are not matched by '*' and '?'
# patterns.

# If recursive is true, the pattern '**' will match any files and
# zero or more directories and subdirectories.
# File:      c:\programdata\miniconda3\lib\glob.py
# Type:      function

##########################################################################
# Signature:
logger.report_table(
    title,
    series,
    iteration=None,
    table_plot=None,
    csv=None,
    url=None,
    extra_layout=None,
)
# Docstring:
# For explicit report, report a table plot.

# One and only one of the following parameters must be provided.

# - ``table_plot`` - Pandas DataFrame or Table as list of rows (list)
# - ``csv`` - CSV file
# - ``url`` - URL to CSV file

# For example:

# .. code-block:: py

df = pd.DataFrame({'num_legs': [2, 4, 8, 0],
        'num_wings': [2, 0, 0, 0],
        'num_specimen_seen': [10, 2, 1, 8]},
        index=['falcon', 'dog', 'spider', 'fish'])

logger.report_table(title='table example',
                    series='pandas DataFrame',
                    iteration=0,
                    table_plot=df)
# You can view the reported tables in the **ClearML Web-App (UI)**, **RESULTS** tab, **PLOTS** sub-tab.

# :param title: The title (metric) of the table.
# :param series: The series name (variant) of the reported table.
# :param iteration: The reported iteration / step.
# :param table_plot: The output table plot object
# :param csv: path to local csv file
# :param url: A URL to the location of csv file.
# :param extra_layout: optional dictionary for layout configuration, passed directly to plotly
#     See full details on the supported configuration: https://plotly.com/javascript/reference/table/
#     example: extra_layout={'xaxis': {'type': 'date', 'range': ['2020-01-01', '2020-01-31']}}
# ##########################################################################
# Signature:
dataset.verify_dataset_hash(
    local_copy_path=None,
    skip_hash=False,
    verbose=False,
)
# Docstring:
# Verify the current copy of the dataset against the stored hash

# :param local_copy_path: Specify local path containing a copy of the dataset,
#     If not provide use the cached folder
# :param skip_hash: If True, skip hash checks and verify file size only
# :param verbose: If True print errors while testing dataset files hash
# :return: List of files with unmatched hashes
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\datasets\dataset.py
# Type:      method
# ##########################################################################
dataset_metods = ['add_external_files',
                'add_files',
                'create',
                'delete',
                'file_entries',
                'file_entries_dict',
                'finalize',
                'get',
                'get_default_storage',
                'get_dependency_graph',
                'get_local_copy',
                'get_logger',
                'get_mutable_local_copy',
                'get_num_chunks',
                'id',
                'is_dirty',
                'is_final',
                'link_entries',
                'link_entries_dict',
                'list_added_files',
                'list_datasets',
                'list_files',
                'list_modified_files',
                'list_removed_files',
                'name',
                'project',
                'publish',
                'remove_files',
                'squash',
                'sync_folder',
                'tags',
                'update_changed_files',
                'upload',
                'verify_dataset_hash']
##########################################################################
# Signature:
dataset.get_local_copy(
    use_soft_links=None,
    part=None,
    num_parts=None,
    raise_on_error=True,
)
# Docstring:
# return a base folder with a read-only (immutable) local copy of the entire dataset
#     download and copy / soft-link, files from all the parent dataset versions

# :param use_soft_links: If True use soft links, default False on windows True on Posix systems
# :param part: Optional, if provided only download the selected part (index) of the Dataset.
#     First part number is `0` and last part is `num_parts-1`
#     Notice, if `num_parts` is not provided, number of parts will be equal to the total number of chunks
#     (i.e. sum over all chunks from the specified Dataset including all parent Datasets).
#     This argument is passed to parent datasets, as well as the implicit `num_parts`,
#     allowing users to get a partial copy of the entire dataset, for multi node/step processing.
# :param num_parts: Optional, If specified normalize the number of chunks stored to the
#     requested number of parts. Notice that the actual chunks used per part are rounded down.
#     Example: Assuming total 8 chunks for this dataset (including parent datasets),
#     and `num_parts=5`, the chunk index used per parts would be:
#     part=0 -> chunks[0,5], part=1 -> chunks[1,6], part=2 -> chunks[2,7], part=3 -> chunks[3, ]
# :param raise_on_error: If True raise exception if dataset merging failed on any file

# :return: A base folder for the entire dataset
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\datasets\dataset.py
# Type:      method
##########################################################################

# Signature:
Dataset.get(
    dataset_id=None,
    dataset_project=None,
    dataset_name=None,
    dataset_tags=None,
    only_completed=False,
    only_published=False,
    auto_create=False,
    writable_copy=False,
)
# Docstring:
# Get a specific Dataset. If only dataset_project is given, return the last Dataset in the Dataset project

# :param dataset_id: Requested Dataset ID
# :param dataset_project: Requested Dataset project name
# :param dataset_name: Requested Dataset name
# :param dataset_tags: Requested Dataset tags (list of tag strings)
# :param only_completed: Return only if the requested dataset is completed or published
# :param only_published: Return only if the requested dataset is published
# :param auto_create: Create new dataset if it does not exist yet
# :param writable_copy: Get a newly created mutable dataset with the current one as its parent,
#     so new files can added to the instance.
# :return: Dataset object
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\datasets\dataset.py
# Type:      method
####################################################################

# Signature:
Dataset.create(
    dataset_name=None,
    dataset_project=None,
    dataset_tags=None,
    parent_datasets=None,
    use_current_task=False,
)
# Docstring:
# Create a new dataset. Multiple dataset parents are supported.
# Merging of parent datasets is done based on the order,
# where each one can override overlapping files in the previous parent

# :param dataset_name: Naming the new dataset
# :param dataset_project: Project containing the dataset.
#     If not specified, infer project name form parent datasets
# :param dataset_tags: Optional, list of tags (strings) to attach to the newly created Dataset
# :param parent_datasets: Expand a parent dataset by adding/removing files
# :param use_current_task: False (default), a new Dataset task is created.
#     If True, the dataset is created on the current Task.
# :return: Newly created Dataset object
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\datasets\dataset.py
# Type:      method
####################################################################
# Signature:
StorageManager.get_local_copy(
    remote_url,
    cache_context=None,
    extract_archive=True,
    name=None,
    force_download=False,
)
# Docstring:
# Get a local copy of the remote file. If the remote URL is a direct file access,
# the returned link is the same, otherwise a link to a local copy of the url file is returned.
# Caching is enabled by default, cache limited by number of stored files per cache context.
# Oldest accessed files are deleted when cache is full.

# :param str remote_url: remote url link (string)
# :param str cache_context: Optional caching context identifier (string), default context 'global'
# :param bool extract_archive: if True returned path will be a cached folder containing the archive's content,
#     currently only zip files are supported.
# :param str name: name of the target file
# :param bool force_download: download file from remote even if exists in local cache
# :return: Full path to local copy of the requested url. Return None on Error.
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\storage\manager.py
# Type:      method