from clearml import Task
dir(Task.TaskTypes)
types = ['application',
        'controller',
        'custom',
        'data_processing',
        'inference',
        'monitor',
        'optimizer',
        'qc',
        'service',
        'testing',
        'training']