import pandas as pd
import os
# from pathlib2 import Path
from clearml import Task, StorageManager

# url for sample data
URL = 's3://recsys/market_sales.csv'
# URL = 'https://www.dropbox.com/s/nhswkhngncsi8n6/Data_rec_sys_row.csv?dl=1'

DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw', 'market_sales.csv')



def load_raw_data(raw_data_url: str):
    """

    :param raw_data_url:
    :return: 
    """
    # create an dataset experiment
task = Task.init(project_name="mlops-rec-sys", task_name="Step 1 load_raw_data m")

# only create the task, we will actually execute it later
# task.execute_remotely(queue_name="default")

# local dataset
artifact = StorageManager.get_local_copy(remote_url=URL, extract_archive=False) #,name='market_sales.csv'

# add and upload local file containing our  dataset
task.upload_artifact('raw-dataset', artifact_object=artifact)

print('Uploading artifacts in the background ...')

# we are done
print('Done')
    
if __name__ == '__main__': 
    
    load_raw_data(URL)    