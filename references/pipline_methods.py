# Signature: 
pipe.add_parameter(name, default=None, description=None, param_type=None)
# Docstring:
# Add a parameter to the pipeline Task.
# The parameter can be used as input parameter for any step in the pipeline.
# Notice all parameters will appear under the PipelineController Task's Hyper-parameters -> Pipeline section
# Example: 
pipeline.add_parameter(name='dataset', 
description='dataset ID to process the pipeline')
# Then in one of the steps we can refer to the value of the parameter with '${pipeline.dataset}'

# :param name: String name of the parameter.
# :param default: Default value to be put as the default value (can be later changed in the UI)
# :param description: String description of the parameter and its usage in the pipeline
# :param param_type: Optional, parameter type information (to used as hint for casting and description)
# File:      c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\automation\controller.py
# Type:      method
#######################################################################
# Type:           PipelineController
# String form:    <clearml.automation.controller.PipelineController object at 0x000002B4D7BC42E0>
# File:           c:\users\lenova\.virtualenvs\rec-sys-retail-adqz8uqy\lib\site-packages\clearml\automation\controller.py
# Docstring:     
# Pipeline controller.
# Pipeline is a DAG of base tasks, each task will be cloned (arguments changed as required), executed, and monitored.
# The pipeline process (task) itself can be executed manually or by the clearml-agent services queue.
# Notice: The pipeline controller lives as long as the pipeline itself is being executed.
# Init docstring:
# Create a new pipeline controller. The newly created object will launch and monitor the new experiments.

# :param name: Provide pipeline name (if main Task exists it overrides its name)
# :param project: Provide project storing the pipeline (if main Task exists  it overrides its project)
# :param version: Must provide pipeline version. This version allows to uniquely identify the pipeline
#     template execution. Examples for semantic versions: version='1.0.1' , version='23', version='1.2'
# :param float pool_frequency: The pooling frequency (in minutes) for monitoring experiments / states.
# :param bool add_pipeline_tags: (default: False) if True, add `pipe: <pipeline_task_id>` tag to all
#     steps (Tasks) created by this pipeline.
# :param str target_project: If provided, all pipeline steps are cloned into the target project.
#     If True pipeline steps are stored into the pipeline project
# :param bool auto_version_bump: If True (default), if the same pipeline version already exists
#     (with any difference from the current one), the current pipeline version will be bumped to a new version
#     version bump examples: 1.0.0 -> 1.0.1 , 1.2 -> 1.3, 10 -> 11 etc.
# :param bool abort_on_failure: If False (default), failed pipeline steps will not cause the pipeline
#     to stop immediately, instead any step that is not connected (or indirectly connected) to the failed step,
#     will still be executed. Nonetheless the pipeline itself will be marked failed, unless the failed step
#     was specifically defined with "continue_on_fail=True".
#     If True, any failed step will cause the pipeline to immediately abort, stop all running steps,
#     and mark the pipeline as failed.
# :param add_run_number: If True (default), add the run number of the pipeline to the pipeline name.
#     Example, the second time we launch the pipeline "best pipeline", we rename it to "best pipeline #2"
##############################################################################
pipe_methods = ['Node',
                'add_function_step',
                'add_parameter',
                'add_step',
                'create_draft',
                'elapsed',
                'get_logger',
                'get_parameters',
                'get_pipeline_dag',
                'get_processed_nodes',
                'get_running_nodes',
                'is_running',
                'is_successful',
                'set_default_execution_queue',
                'set_pipeline_execution_time_limit',
                'start',
                'start_locally',
                'stop',
                'update_execution_plot',
                'upload_artifact',
                'upload_model',
                'valid_job_status',
                'wait']
##############################################################################