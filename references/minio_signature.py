# Init signature:
Minio(
    endpoint,
    access_key=None,
    secret_key=None,
    session_token=None,
    secure=True,
    region=None,
    http_client=None,
    credentials=None,
)
# Docstring:     
# Simple Storage Service (aka S3) client to perform bucket and object
# operations.

# :param endpoint: Hostname of a S3 service.
# :param access_key: Access key (aka user ID) of your account in S3 service.
# :param secret_key: Secret Key (aka password) of your account in S3 service.
# :param session_token: Session token of your account in S3 service.
# :param secure: Flag to indicate to use secure (TLS) connection to S3
#     service or not.
# :param region: Region name of buckets in S3 service.
# :param http_client: Customized HTTP client.
# :param credentials: Credentials provider of your account in S3 service.
# :return: :class:`Minio <Minio>` object

# Example::
    # Create client with anonymous access.
client = Minio("play.min.io")

# Create client with access and secret key.
client = Minio("s3.amazonaws.com", "ACCESS-KEY", "SECRET-KEY")

# Create client with access key and secret key with specific region.
client = Minio(
    "play.minio.io:9000",
    access_key="Q3AM3UQ867SPQQA43P2F",
    secret_key="zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
    region="my-region",
)

# **NOTE on concurrent usage:** `Minio` object is thread safe when using
# the Python `threading` library. Specifically, it is **NOT** safe to share
# it between multiple processes, for example when using
# `multiprocessing.Pool`. The solution is simply to create a new `Minio`
# object in each process, and not share it between processes.

####################################################################
#    client methods
['bucket_exists',
'compose_object',
'copy_object',
'delete_bucket_encryption',
'delete_bucket_lifecycle',
'delete_bucket_notification',
'delete_bucket_policy',
'delete_bucket_replication',
'delete_bucket_tags',
'delete_object_lock_config',
'delete_object_tags',
'disable_accelerate_endpoint',
'disable_dualstack_endpoint',
'disable_object_legal_hold',
'disable_virtual_style_endpoint',
'enable_accelerate_endpoint',
'enable_dualstack_endpoint',
'enable_object_legal_hold',
'enable_virtual_style_endpoint',
'fget_object',
'fput_object',
'get_bucket_encryption',
'get_bucket_lifecycle',
'get_bucket_notification',
'get_bucket_policy',
'get_bucket_replication',
'get_bucket_tags',
'get_bucket_versioning',
'get_object',
'get_object_lock_config',
'get_object_retention',
'get_object_tags',
'get_presigned_url',
'is_object_legal_hold_enabled',
'list_buckets',
'list_objects',
'listen_bucket_notification',
'make_bucket',
'presigned_get_object',
'presigned_post_policy',
'presigned_put_object',
'put_object',
'remove_bucket',
'remove_object',
'remove_objects',
'select_object_content',
'set_app_info',
'set_bucket_encryption',
'set_bucket_lifecycle',
'set_bucket_notification',
'set_bucket_policy',
'set_bucket_replication',
'set_bucket_tags',
'set_bucket_versioning',
'set_object_lock_config',
'set_object_retention',
'set_object_tags',
'stat_object',
'trace_off',
'trace_on']
 
###################################################################
#  Signature:
MINIO_CLIENT.fget_object(
    bucket_name,
    object_name,
    file_path,
    request_headers=None,
    ssec=None,
    version_id=None,
    extra_query_params=None,
    tmp_file_path=None,
)
# Docstring:
# Downloads data of an object to file.

# :param bucket_name: Name of the bucket.
# :param object_name: Object name in the bucket.
# :param file_path: Name of file to download.
# :param request_headers: Any additional headers to be added with GET
#                         request.
# :param ssec: Server-side encryption customer key.
# :param version_id: Version-ID of the object.
# :param extra_query_params: Extra query parameters for advanced usage.
# :param tmp_file_path: Path to a temporary file.
# :return: Object information.
###################################################################