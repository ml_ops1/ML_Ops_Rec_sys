import os
from clearml import Dataset

DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw', 'market_sales.csv')
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim', 'data_cleaned.csv')

dataset_name = "raw-dataset-v1"
dataset_project = "mlops-rec-sys"

dataset_path = Dataset.get(
    dataset_name=dataset_name, 
    dataset_project=dataset_project).get_local_copy()

Dataset.create(
    dataset_name=None,
    dataset_project=None,
    dataset_tags=None,
    parent_datasets=None,
    use_current_task=False,
)
logger.report_table(
    title="Trainset - after filling missing values",
    series="pandas DataFrame",
    iteration=0,
    table_plot=train_set.head(),
)

task.upload_artifact("Outcome dictionary", outcome_dict)

task = Task.init(project_name="Table Example", task_name="tabular preprocessing")
logger = task.get_logger()
configuration_dict = {"test_size": 0.1, "split_random_state": 0}
configuration_dict = task.connect(
    configuration_dict
)

task.upload_artifact("Processed data", paths)
task.upload_artifact("train_data", artifact_object=train_df)
task.upload_artifact("val_data", artifact_object=val_df)
